REPO_BASE=https://Hikki12@bitbucket.org/Hikki12/python-mockupio.git
REPO_FOLDER=python-mockupio
VIRTUAL_ENV_NAME=venv


folder_exists(){
    if [ -d "$1" ]; then
        #echo "Folder $1 exists!" 
        return $(true)
    else
        #echo "Folder $1 doesn't exists!" 
        return $(false)
    fi
}

set_env(){
    source $VIRTUAL_ENV_NAME/bin/activate
}

create_env(){
    if folder_exists $VIRTUAL_ENV_NAME; then 
        return
    else
        python3 -m venv $VIRTUAL_ENV_NAME
    fi
}


configure_env(){
    create_env
    set_env
}

clone_repo(){
    echo "Configuring repo..."
    if folder_exists $REPO_FOLDER; then
        cd $REPO_FOLDER
        git submodule update --init --recursive --force
        cd ..
        return
    fi
}

configure_repo(){
    clone_repo
}

configure_env
configure_repo