/*
* Author: Jason Francisco Macas Mora
* Email : franciscomacas3@gmail.com
* License: MIT
*/

#include <ArduinoJson.h>
#include <SimpleTimer.h>
#include <AccelStepper.h>


/*
Events:
  Recived:
    $stop: it will stop experiment
    $status: it asks for the current state
  Emitted: 
    $recived -> it notifies the data were recived
    $status, ok -> response status of the system is ok
    $status, error:msg  -> response system has some error
*/

/* PINS AREA  --------------------------------------------------------------------------------*/

// Sensors Pins
const int sensor1Pin = A0;
const int sensor2Pin = A1;
const int sensor3Pin = A2;

// Motor pins
const int motorDirPin = 6;
const int motorStepPin = 7;
const int motorIsEnablePin = 5;

AccelStepper stepper = AccelStepper(AccelStepper::DRIVER, motorStepPin, motorDirPin);

// Origin sensor for set vertically
const int originSensorPin = 8;

// Magnet Pins
const int magnetPin = 9;

/* VARIABLES FOR SERIAL ---------------------------------------------------------------------*/

String inputString = "";         // a String to hold incoming data
String event = "";
bool stringComplete = false;  // whether the string is complete

/* VARIABLES AREA ----------------------------------------------------------------------------*/

// Initialize JSON object *
StaticJsonDocument<512> doc;

// JSON variables {

bool playState = false;
unsigned long dt1 = 0;
unsigned long dt2 = 0;
unsigned long dt3 = 0;

// }

// Auxiliar Variables *
// These variables will be helpful for monitoring the mockup use
bool isInUse = false;

// Internal Variables *

// Magnet
bool magnetIsOn = false;

// Motor
bool motorIsEnable = true;
const int defaultSpeed = 480;
const int defaultAceleration = 400;

// Measurement

unsigned long t0 = 0;
unsigned long t1 = 0;
unsigned long t2 = 0;
unsigned long t3 = 0;

unsigned long maxMeasurementTime = 7000000; //uS

bool measuring = false;
bool sensorState1 = false;
bool sensorState2 = false;
bool sensorState3 = false; 



unsigned int sensorIndex = 0;

// originSensor

int originSensorFalsePulses= 0;
bool originSensorState = false;
bool checkForOrigin = true;

// For experiment

const int catchPosition = 7650;

bool ballIsCatched = false;
bool ballWasDropped = false;
bool originIsSet = false;
bool originWasSet = false;

bool isVertical = false;

bool runVertical = false;
bool runCatch = false;

bool experimentIsRunning = false;
bool experimentStarted = false;
bool experimentEnded = false;
bool experimentCanRun = false;

bool catchOk = false;

/* TIMERS AREA --------------------------------------------------------------------------------*/

// * STOP TIMER *
const long stopInterval = 60000; //ms
const int maxStopMinutes = 2; 
int stopMinutes = 0;

SimpleTimer stopTimer(stopInterval);

void autoStop(){

  if(stopTimer.isReady()){
    stopMinutes++;
    stopTimer.reset();
  }

  if(stopMinutes >= maxStopMinutes){
    stop();
    stopMinutes = 0;
  }

}

void resetStopTimer(){
  stopMinutes = 0;
  stopTimer.reset();
}

// ****
const long maxWaitTime = 12000; //ms
SimpleTimer waitTimer(maxWaitTime);

const long maxSetVerticalTime = 60000; // ms
SimpleTimer setVerticalTimer(maxSetVerticalTime);

const long maxCatchTime = 12000; // ms
SimpleTimer catchBallTimer(maxCatchTime);


const int experimentEndedTime = 5000; //ms
SimpleTimer experimentEndedTimer(experimentEndedTime);

/* SEND/READ INFO -----------------------------------------------------------------------------*/

void sendVariables(){
  doc["PlayState"] = playState;
  doc["dt1"] = dt1;
  doc["dt2"] = dt2;
  doc["dt3"] = dt3;
  doc["catched"] = catchOk;
  serializeJson(doc, Serial);  
  Serial.println();
  catchOk = false;
}

void dataRecived(){
  // Notify originSensorFalsePulseshave recived the data
  Serial.println("$recived");
}

void readVariables(){
  
  DeserializationError error = deserializeJson(doc, inputString);
  
  if(error){
    // Serial.println(error.f_str());
    return;
  }
  // Set Variables
  playState = doc["PlayState"];

  // dt1 = doc["dt1"];
  // dt2 = doc["dt2"];
  // dt3 = doc["dt3"];

  dataRecived();

  configureExperiment();

}

void serialEvent() {
  while (Serial.available()) {
    char inChar = (char) Serial.read();
    inputString += inChar;

    if (inChar == '\n') {
      if(inputString[0] == '$'){
        readEvents();
      }
      if(inputString[0] == '{'){
        readVariables();
      }
      
      stringComplete = true;
      inputString = "";
      event = "";
    }
  }
}

void readEvents(){
    event = inputString.substring(1);
    event.trim();
    Serial.print("event: ");
    Serial.println(event);
    
    if(event == "stop"){
      stop();
      return;
    }
    
    if(event == "v"){
      RunVerticalMode();
      Serial.println("Run Vertical!");
      return;
    }
    
    if(event == "c"){
      RunCatchMode();
      Serial.println("Run Catch!");
      return;
    }

    if(event == "i"){
      experimentIsRunning = true;
      Serial.println("Start Running!");
      return;
    }
    
    if(event == "d"){
      dropBall();
      Serial.println("Dropping ball!");
      return;
    }

    if(event == "e"){
      magnetIsOn = true;
      Serial.println("Magnet on!");
      return;
    }

    if(event == "status"){
      areYouOk();
      return;
    }
}

/* CHECK SYSTEM ---------------------------------------------------------------------------------*/

void areYouOk(){
  bool ok = true;
  String error = "";
  
  // check if are there any error on the system
  if(ok){
    Serial.println("$status,ok");
  }else{
    Serial.println("$status,error: " + error);
  }
}

/* SYSTEM FUNCTIONS -----------------------------------------------------------------------------*/

void configureExperiment(){
  isInUse = true;
  resetStopTimer();

  if(playState){
    playState = false;
    if(!experimentIsRunning){
      experimentIsRunning = true;
    }
  }

}

void dropBall(){

  magnetIsOn = false;
  ballIsCatched = false;
  ballWasDropped = true;
  experimentEnded = false; 

  if(!experimentStarted)
    experimentStarted = true;

}

void measureFall(){

  // Si el imán fue apagado y la esfera se soltó inciar medición
   if(!magnetIsOn && ballWasDropped){  
      t0 = micros();
      measuring = true;
      sensorIndex++;
      experimentEndedTimer.reset();
      ballWasDropped = false;
   }

   // Si el sensor 1 detecta un objeto, registrar el tiempo 1
   if(sensorState1 && sensorIndex == 1){
      t1 = micros();
      sensorIndex++;
   }

   // Si el sensor 2 detecta un objeto, registrar el tiempo 2
   if(sensorState2 && sensorIndex == 2){
      t2 = micros();
      sensorIndex++;
   }
   // Si el sensor 3 detecta un objeto, registra el tiempo 3
   if(sensorState3 && sensorIndex == 3){
      t3 = micros();
      dt1 = t1 - t0;
      dt2 = t2 - t1; 
      dt3 = t3 - t2;

      sensorIndex = 0;

      measuring = false;
      experimentStarted = false;
      experimentEnded = true;
      experimentEndedTimer.reset();
      sendVariables();
   }

   // Si el ciclo de experimento no se completa en el orden esperado, notificar error y reiniciar
   if (measuring){
      if(micros() - t0 >= maxMeasurementTime){
        t0 = 0;
        sensorIndex = 0;
        measuring = false;
        playState = false;
        experimentStarted = false;
        experimentEnded = true;
        experimentEndedTimer.reset();
        Serial.println("$error: failed to read sensors");  
      }
    }

}

void RunVerticalMode(){
  runVertical = true;
  runCatch = false;
}

void RunCatchMode(){
  
  runVertical = false;
  runCatch = true;
}

void setVertical(){

  stepper.setSpeed(defaultSpeed);
  
  if(originIsSet){
    stepper.setCurrentPosition(0);
    isVertical = true;
    runVertical = false;
    runCatch = false; 
    originIsSet = false;
    originWasSet = true;
    checkForOrigin = false;
    originSensorFalsePulses= 0;
    return;
  }

  // if(setVerticalTimer.isReady()){
  //   Serial.println("$error: max time to set vertical");
  //   runVertical = false;
  //   runCatch = false;
  //   originIsSet = false;
  //   originWasSet = false;
  //   originSensorFalsePulses = 0;
  //   return;
  // }

  stepper.runSpeed(); 
}

void checkingBallReturn(){

    if(sensorState1){
      ballIsCatched = true;
      ballWasDropped = false;
      runCatch = false;
      magnetIsOn = true;
      catchOk = true;
      Serial.println("Ball catched!");
      catchBallTimer.reset();
      originWasSet = false;
      return;
    }

    if(catchBallTimer.isReady()){
      ballIsCatched = false;
      ballWasDropped = false;
      RunVerticalMode();
      magnetIsOn = false;
      Serial.println("$error: Ball not detected! :c");
      catchBallTimer.reset();
      originWasSet = false;
      return;
    }

}

void catchBall(){
  
  isVertical = false;
  checkForOrigin = true;
  originSensorFalsePulses= 0;
  
  stepper.moveTo(-catchPosition);
  stepper.setSpeed(-defaultSpeed);
 
  if(stepper.distanceToGo() != 0){
  
    stepper.runSpeedToPosition();  
    catchBallTimer.reset();
  
  }else{
    
    checkingBallReturn();
  
  }
  
}

void readSensors(){
  // Leer sensores 
  sensorState1 = !(bool)digitalRead(sensor1Pin);  
  sensorState2 = !(bool)digitalRead(sensor2Pin);
  sensorState3 = !(bool)digitalRead(sensor3Pin);
}

void preparateExperiment(){

  if(!experimentStarted && !experimentEnded){

    if(!originWasSet){
      
      RunVerticalMode();  
      
    }else{
      
      if(ballIsCatched) 
        RunVerticalMode();
      else
        RunCatchMode();  
      
    }
  }

}

void resetExperiment(){
  //Serial.println("Restarting...");
  RunVerticalMode();
  magnetIsOn = false;
  experimentEnded = false;
  experimentStarted = false;
  experimentIsRunning = false;
  t0 = 0;
  t1 = 0;
  t2 = 0;
  t3 = 0;
  dt1 = 0;
  dt2 = 0;
  dt3 = 0;
  experimentEndedTimer.reset();
}

void runExperiment(){

  // Verificar que la esfera esté agarrada y el eje esté vertical
  experimentCanRun = ballIsCatched && isVertical;
  
  if(experimentIsRunning){

    if(experimentCanRun)
      dropBall();
    else
      preparateExperiment();
      
  }

  if(experimentStarted)
    measureFall();

  if(experimentEnded)
    resetExperiment();

}

void stop(){
  playState = false;
  isInUse=false;
  resetStopTimer();
  resetExperiment();
  sendVariables();
  
}

void motorEnableControl(){
  
  motorIsEnable = runCatch || runVertical || !isVertical;
  digitalWrite(motorIsEnablePin, LOW);

}

void checkOrigin(){
  originSensorState = !(bool)digitalRead(originSensorPin);
  
  if(originSensorState && checkForOrigin){
    originSensorFalsePulses++;
    originIsSet = false;
  }

  if(!runVertical)
    originSensorFalsePulses = 0;
    
  if(originSensorFalsePulses > 30){
    Serial.println("origin Detected!!");
    originIsSet = true;
    checkForOrigin = false;
    originSensorFalsePulses = 0;
  }
}


void motorControl(){

  motorEnableControl();
  checkOrigin();   

  if(!isVertical && runVertical)
    setVertical();   

  if(!ballIsCatched && runCatch)
    catchBall();

}

void magnetControl(){
  digitalWrite(magnetPin, magnetIsOn);  
}

/* SET UP -----------------------------------------------------------------------------------------*/

void configurePins(){
  pinMode(sensor1Pin, INPUT_PULLUP);
  pinMode(sensor2Pin, INPUT_PULLUP);
  pinMode(sensor3Pin, INPUT_PULLUP);
  pinMode(originSensorPin, INPUT);
  pinMode(magnetPin, OUTPUT);
  pinMode(motorIsEnablePin, OUTPUT);
}

void setup() {
  // Configure Serial
  Serial.begin(9600);
  inputString.reserve(256);
  event.reserve(256);

  // Configure Pins
  configurePins();

  // Configure motor params
  stepper.setMaxSpeed(defaultSpeed);
  stepper.setAcceleration(defaultAceleration); 
}

void loop() {
  autoStop();
  magnetControl();
  readSensors();
  runExperiment();
  motorControl();
}
