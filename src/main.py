import sys
import os
import pyqtgraph as pg
from PyQt5 import uic
from PyQt5.QtWidgets import QMainWindow, QApplication, QTableWidgetItem
import json
import time
sys.path.append(os.path.join(os.path.dirname(__file__), '../python-mockupio'))
from mockupio import MockupClient
from options import * 
from camera_utils import preprocess, toPixmap
from parabolic_fit import get_signal, get_string_equation
from tester import Tester

ui_path = os.path.dirname(os.path.abspath(__file__))
ui_file = os.path.join(ui_path, 'GUI.ui')


class Mockup(QMainWindow):
    """"Mockup.
    Add some description here.
    """
    def __init__(self, 
            camera_options, 
            serial_options, 
            socketio_options, 
            extra_options,
            **kwargs):
        super().__init__()
        uic.loadUi('GUI.ui', self)
        self.__set_graph_settings()
        # SET VARIABLES ----------------------------------------------------------------------
        self.variables = {
            "PlayState": False,
            "Restart": False,
            "dt1": 0.28,
            "dt2": 0.12,
            "dt3": 0.089,
            "equation": "$y(t) =  - 8.1786t^{2}  + 1.5651t  + 1.8815$"
        }
        # INITIALIZE CLIENT ------------------------------------------------------------------

        self.client = MockupClient(**camera_options, **serial_options, **socketio_options, **extra_options)

        # MOCKUP CALLBACKS --------------------------------------------------------------------

        self.client.on('camera-image-ready', self.show_image)
        self.client.on('camera-record-end', self.record_end)

        self.client.on('serial-connection-event', self.handle_serial_connection_status)
        self.client.on('serial-incoming-data', self.recive_data_serial)

        self.client.on('socketio-connection-event', self.handle_socketio_connection_status)
        self.client.on('socketio-request-updates', self.response_to_server)
        self.client.on('socketio-incoming-data', self.recive_data_socketio)
        self.client.on('socketio-stop-event', self.handle_stop_event_socketio)
        self.client.on('socketio-busy', self.handle_busy_event)

        self.client.on('error', self.handle_errors)
        
        # MULTIPROCESSING CALLBACKS ----------------------------------------------------------

        # self.params = self.client.obtain_dict()
        # self.params["color"] = 1
        self.client.camera.set_callback('preprocess', preprocess)
        # self.client.camera.set_callback('after-read', process_image, **self.params)

        # START  MOCKUP CLIENT ---------------------------------------------------------------------

        self.client.start(camera=True, serial=True, socketio=True)

        # READ GUI EVENTS -------------------------------------------------------------------------
        self.PlayState.clicked.connect(lambda: self.gui_events(id='PlayState', value=self.PlayState.isChecked()))
        self.catchBtn.clicked.connect(lambda: self.client.write_to_serial('$c', toJson=False))
        self.verticalBtn.clicked.connect(lambda: self.client.write_to_serial('$v', toJson=False))
        self.dropBtn.clicked.connect(lambda: self.client.write_to_serial('$d', toJson=False))
        self.stopBtn.clicked.connect(lambda: self.client.write_to_serial('$stop', toJson=False))
        self.tester = Tester(folder="tests", n_tests=100, running=False)
        #
        self.params_eqt = None
        # time.sleep(1)
        # if self.tester.is_running():
        #     self.tester.start_test()
        #     self.client.write_to_serial("$i", toJson=False)

        #self.Restart.clicked.connect(lambda: self.gui_events(id='Restart', value=self.Restart.isChecked()))

    def __set_graph_settings(self):
        self.graph.setBackground((22, 24, 30))
        self.graph.setRange(xRange=(-0.55, 0.55), yRange=(-0.1, 1.6))
        self.graph.showGrid(x=True, y=True)
        self.pen = pg.mkPen(width=2, color=(200, 10, 10))
        self.signal_line = self.graph.plot(pen=self.pen)
        self.markers_points = self.graph.plot(pen=None, symbol='o')
        self.graph.setLabel(axis='left', text='h(t)')
        self.graph.setLabel(axis='bottom', text='tiempo |seg|')

    def set_equation(self, eqt_str):
        self.equationLabel.setText(eqt_str)

    def set_gvalue(self, eqt_params):
        g = abs(2*eqt_params[0])
        g_str = "{:.4f}".format(g)
        self.gLabel.setText(g_str)

    def plot_data(self, t, y):
        self.signal_line.clear()
        self.signal_line.setData(t, y)

    def plot_markers(self, t, y):
        self.markers_points.clear()
        self.markers_points.setData(t, y)

    def update_table(self, data):
        horHeaders = []
        for n, key in enumerate(sorted(data.keys())):
            horHeaders.append(key)
            for m, item in enumerate(data[key]):
                value = "{:.4f}".format(item)
                newitem = QTableWidgetItem(value)
                self.table.setItem(m, n, newitem)
        self.table.setHorizontalHeaderLabels(horHeaders)

    def process_experiment(self, data:dict):
        dt_vector = [data["dt1"], data["dt2"], data["dt3"]]
        time, signal, signal_sampled, time_sampled, equation_params = get_signal(dt_vector, scale=10**6)
        self.params_eqt = equation_params
        eqt = get_string_equation(equation_params)
        self.variables["equation"] = eqt
        data = {
            "t": time_sampled,
            "h(t)": signal_sampled
        }
        print(data)
        self.update_table(data)
        self.plot_data(t=time, y=signal)
        self.plot_markers(t=time_sampled, y=signal_sampled)
        self.set_equation(eqt)
        self.set_gvalue(equation_params)


    def gui_events(self, id, value, send=True,toSerial=True, toServer=True):
        self.client.backup(self.variables)
        self.variables[id] = value
        if send:
            if id=="PlayState":
                self.client.write_to_serial("$i", toJson=False)
            self.client.write(self.variables, toSerial=toSerial, toServer=toServer)

    def set_to_gui(self, _id:str, value):
        try:
            attribute = getattr(self, _id)
            if isinstance(value, bool):
                attribute.setChecked(value)
                return
            if isinstance(value, int):
                attribute.setValue(value)
                return
        except Exception as e:
            print(e)

    def update_variables(self, data):
        if isinstance(data, dict):
            for _id in data:
                if _id in self.variables:
                    value = data[_id]
                    self.variables[_id] = value
                    self.set_to_gui(_id, value)
        
    def show_image(self, frame, results):
        pixmap = toPixmap(frame)
        self.image.setPixmap(pixmap)
    
    def record_end(self):
        print("Recording camera ended!")

    def handle_serial_connection_status(self, status):
        self.ledSerial.setChecked(status)
        print("Serial connection status: ", status) 

    def recive_data_serial(self, data):
        #print("Serial says: ", data)    
        if isinstance(data, str):
            if data[0] != "$":
                try:
                    variables = json.loads(data)
                    self.process_experiment(variables)
                    self.update_variables(variables)
                    variables["params_eqt"] = self.params_eqt
                    self.client.write(self.variables, toSerial=False)
                    if self.tester.is_running():
                        self.tester.end_test(variables)
                        time.sleep(1)
                        print("Starting Test...")
                        self.tester.start_test()
                        self.PlayState.setChecked(True)
                        self.gui_events(id='PlayState', value=self.PlayState.isChecked())
                except Exception as e:
                    data = {}
                    #self.tester.end_test(data, error=e)
                    #time.sleep(2)
                    print(e)
            else:
                print("event: ", data)
    
    def handle_stop_event_socketio(self):
        print("stoping mockup")
        self.client.write_to_serial("$stop", toJson=False)

    def handle_socketio_connection_status(self, status):
        self.ledOnline.setChecked(status)
        print("socketio connection status: ", status) 
    
    def handle_busy_event(self, status):
        print("Is busy?: ", status)

    def response_to_server(self):
        print("Responding to server ...")
    
    def recive_data_socketio(self, data):
        print("Socketio recives: ", data)
        self.update_variables(data)
        self.client.write(data, toServer=False)
    
    def handle_errors(self, error):
        print(error)
    
    def closeEvent(self, e):
        self.client.stop()

if __name__ == '__main__':
    app = QApplication(sys.argv)
    w = Mockup(camera_options, serial_options, socketio_options, extra_options)
    w.show()
    sys.exit(app.exec_())