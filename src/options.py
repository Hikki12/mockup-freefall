camera_options = {
    "index_device": 1,  # Index camera device
    "size": (700, 600),  # Image size
    "quality": 90,  # jpeg quality
    "fps_record": 10, # fps speed of capture
    "h_flip": True, # flip horizontally?
    "v_flip": False # flip vertically?

}

serial_options = {
    "port": None, # a str name, if you give None it will connect to the first device founded.
    "baudrate": 9600, # give a baudrate
    "timeout": 0.25, # seconds
    "reconnection_time_serial": 1 # seconds
}

socketio_options = {
    "server_address": "https://labremoto-beta.herokuapp.com", # Change this with your server address
    "identifier": "MAQUETA-FREEFALL", # an ID
    "fps_stream": 7, # fps speed of streaming
    "reconnection_time_socketio": 2, # seconds
    "auto_stream": True, # If it's True it will control streaming of video automatically, else it will be ever streaming
    "stream_to_host": False, # if it's True it will stream directlly to the server address, else it will autoadmin streaming namespace
    "latency": 0.005, # latency in seconds
    "write_delay": 0.33 # seconds
}

extra_options = {
    "multiprocess_camera": True, # if it's True, a new process will be create to manage the Camera
    "multiprocess_serial": True, # if it's True, a new process will be create to manage the Serial
    "multiprocess_socketio": True, # if it's True, a new process will be create to manage the Socketio
    "debug_camera": False, # to enable logger of camera
    "debug_serial": True, # to enable logger of serial
    "debug_socketio": False, # to enable logger of socketio
    "engine_logger":False, # gives details from engine-io
    "socketio_logger": False # gives details from socket-io
}
