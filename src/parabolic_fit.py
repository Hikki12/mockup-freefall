#import matplotlib.pyplot as plt
import numpy as np

"""Module for make a parabolic fit of points
  ------ h0
    |
    |
    |    dh1 
    |   
    |
  ------ h1
    |   
    |    
    |   dh2
    |
    |
  ------ h2
    |   
    |
    |    dh3   
    |
    |
  ------ h3
    |   
    |    
    |
    |

  h1 = h0 - dh1
  h2 = h1 - dh2 
  h3 = h3 - dh3
                
"""
# Conditions
R = 0.00
h0 = 1.8
dh1 = 0.18 - R
dh2 = 0.49 - R
dh3 = 0.51 - R

h1 = h0 - dh1
h2 = h1 - dh2
h3 = h2 - dh3
y = [h0, h1, h2, h3]


def get_time_vector(dt_vector=None, scale=1):
    """It returns a time vector from a time sampling vector (dt_vector)

    Time scale:

            dt1               dt2          dt3
    |-------------------|--------------|----------|-----|
    t0                  t1             t2         t3

    t0 = 0
    t1 = t0 + dt1
    t2 = t1 + dt2
    t3 = t2 + dt3

    time_vector = [t0, t1, t2, t3]
    """
    if dt_vector is None:
        raise Exception("Invalid dt vector! Please check it")
    dt1, dt2, dt3 = dt_vector
    dt1 /= scale
    dt2 /= scale
    dt3 /= scale
    t0 = 0
    t1 = t0 + dt1
    t2 = t1 + dt2
    t3 = t2 + dt3
    return [t0, t1, t2, t3]


def parabolic_fit(t=None, y=None):
    equation_params = np.polyfit(t, y, 2)
    return equation_params


def get_string_equation(equation_params, n_decimals=4, latex=True):
    a, b, c = equation_params
    prefix = "{:."+str(n_decimals)+"f}"
    if a >= 0:
        a_string = ("   " + prefix).format(abs(a))
    else:
        a_string = (" - " + prefix).format(abs(a))
    if b >= 0:
        b_string = (" + " + prefix).format(abs(b))
    else:
        b_string = (" - " + prefix).format(abs(b))
    if c >= 0:
        c_string = (" + " + prefix).format(abs(c))
    else:
        c_string = (" - " + prefix).format(abs(c))

    if not latex:
        equation_string = "y(t) = " + a_string + "t^2" + b_string + "t" + c_string
    else:
        equation_string = "$y(t) = {0}t^{{2}} {1}t {2}$".format(a_string, b_string, c_string)
    return equation_string


def reconstruct_signal(equation_params=None, n_samples=100, xlim=(-0.55, 0.55)):
    if equation_params is None:
        raise Exception('Equation params cannot be None type! Please check it.')

    lim1, lim2 = xlim
    time = np.linspace(lim1, lim2, n_samples)
    a, b, c = equation_params
    signal = a * (time ** 2) + b * time + c
    return time, signal


def get_signal(dt_vector=None, scale=1):
    """It returns the data processed"""
    global y
    signal_sampled = y
    time_sampled = get_time_vector(dt_vector, scale=scale)
    equation_params = parabolic_fit(t=time_sampled, y=signal_sampled)
    time, signal = reconstruct_signal(equation_params, xlim=(-0.55, 0.55))
    return time, signal, signal_sampled, time_sampled, equation_params


# dt_vector = [0.28, 0.12, 0.089]
# time, signal, signal_sampled, time_sampled, equation_params = get_signal(dt_vector)
# g = 2*equation_params[0]
# print("g: {:.3f} ".format(abs(g)))
# eqt = get_string_equation(equation_params, n_decimals=4)
# print(eqt)

# plt.plot(time_sampled, signal_sampled, 'o')
# plt.plot(time, signal)
# plt.grid()
# plt.show()
