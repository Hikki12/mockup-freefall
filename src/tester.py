import numpy as np 
import os
import time


class Tester:
    def __init__(self, folder="tests", prefix="test", n_tests=10, running=True):
        self.i = 1
        self.folder = folder
        self.prefix = prefix
        self.n_tests = n_tests
        self.running = running
        self.t0 = 0
        self.t1 = 0
        self.check_folder()
    
    def check_folder(self):
        if not os.path.exists(self.folder):
            os.mkdir(self.folder)

    def start_test(self):
        self.t0 = time.time()
        print(f"->Test{self.i} started.")

    def end_test(self, data, error=None):
        if self.running:
            self.t1 = time.time()
            test_name = f"{self.prefix} - {self.i}"
            file = f"{test_name}.npy"
            filename = os.path.join(self.folder, file)
            ellapsed = self.t1 - self.t0
            self.i += 1
            try:
                if isinstance(data, dict):
                    data["ellapsed"] = ellapsed
                    data["error"] = error
                print(f"-->Saving: {test_name} || error: {error}")
                np.save(filename, data)
            except Exception as e:
                print("ERROR", e)
            if self.i > self.n_tests:
                self.stop()
                
    def stop(self):
        self.running = False
        print("===== TESTS COMPLETED ===== ")
    
    def is_running(self):
        return self.running