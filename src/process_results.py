import numpy as np
import os 
import xlsxwriter


def get_time(dt1, dt2, dt3, scale=10**6):
    t0 = 0
    t1 = t0 + dt1/scale
    t2 = t1 + dt2/scale
    t3 = t2 + dt3/scale
    return t1, t2, t3 


path = "./tests"
files = os.listdir(path)
results = []

for file in files:
    filename = os.path.join(path, file)
    result = np.load(filename, allow_pickle=True)
    results.append(result)


# Create a workbook and add a worksheet.
workbook = xlsxwriter.Workbook('Results.xlsx')
worksheet = workbook.add_worksheet()


# Start from the first cell. Rows and columns are zero indexed.
row = 0
col = 0
i = 1
row_offset = 2
col_offset = 1

worksheet.write(row + row_offset - 1, col + 1, '#')
worksheet.write(row + row_offset - 1, col + 2, 'Correcto')
worksheet.write(row + row_offset - 1, col + 3, 't1')
worksheet.write(row + row_offset - 1, col + 4, 't2')
worksheet.write(row + row_offset - 1, col + 5, 't3')
worksheet.write(row + row_offset - 1, col + 6, 'g med.')


for result in results:
    r = result.item()
    params = r["params_eqt"]
    catched = int(r["catched"])
    t1, t2, t3 = get_time(r["dt1"], r["dt2"], r["dt3"])
    g = abs(2*params[0])
    worksheet.write(row + row_offset, col+1, i)
    worksheet.write(row + row_offset, col+2, catched)
    worksheet.write(row + row_offset, col+3, t1)
    worksheet.write(row + row_offset, col+4, t2)
    worksheet.write(row + row_offset, col+5, t3)
    worksheet.write(row + row_offset, col+6, g)
    i += 1
    row += 1

workbook.close()


