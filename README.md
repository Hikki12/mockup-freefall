# Python-mockup-template

This is an example for a python mockup.

# Description

Add some description here...

# Project Estructure
The project has three principal folders:
```
|-- arduino
|       |-- template.ino
|-- src
|   |-- main.py
|   |-- options.py
|   |-- camera_utils.py
|-- python-mockupio
|-- .gitignore
|-- .gitmodules
|-- configure.sh
|-- README.md
|-- run.sh
|-- update.sh
```
**src** folder has your code, here you have a template, you should edit and add what you want.
<br/>
**arduino** folder has the arduino code template.
<br/>
**python-mokupio** is the base library.

# Installation

First of all clone this repository. After you should run the bash script to auto-configure your enviroment work. Don't forget to give access to the file:

```
# Give access to the file
sudo chmod u+x configure.sh

# Excute the file
. ./configure.sh
```

# Run

You should run the code with the virtualenv:

```
# Load virtualenv
source venv/bin/activate

# Set directory
cd src/

# Run the script
python main.py
```

# Options

You could configure some parameters, this configurations are on **src/options.py** file:

## Serial

```
serial_options = {
    "port": None, # a str name, if you give None it will connect to the first device founded.
    "baudrate": 9600, # give a baudrate
    "timeout": 0.25, # seconds
    "reconnection_time_serial": 1 # seconds
}
```

## Camera

```
camera_options = {
    "index_device": 0,  # Index camera device
    "size": (320, 240),  # Image size
    "quality": 90,  # jpeg quality
    "fps_record": 10 # fps speed of capture
}
```

## Socket-io

```
socketio_options = {
    "server_address": "https://localhost:3000", # Change this with your server address
    "identifier": "MAQUETA-TEMPLATE", # an ID
    "fps_stream": 8, # fps speed of streaming
    "reconnection_time_socketio": 1, # seconds
    "auto_stream": False, # If it's True it will control streaming of video automatically, else it will be ever streaming
    "stream_to_host": False # if it's True it will stream directlly to the server address, else it will autoadmin streaming namespace
}

```

## Extra

```
extra_options = {
    "multiprocess_camera": True, # if it's True, a new process will be create to manage the Camera
    "multiprocess_serial": False, # if it's True, a new process will be create to manage the Serial
    "multiprocess_socketio": False, # if it's True, a new process will be create to manage the Socketio
    "debug_camera": True, # to enable logger of camera
    "debug_serial": True, # to enable logger of serial
    "debug_socketio": True # to enable logger of socketio
}

```

# Arduino

You could use the Arduino template include in the folder **arduino**.

# Updates

To check for updates of the python mockup-library do:

```
# Give acces
sudo chmod u+x update.sh

# Run
./update.sh
```

# Create a repository

If you want to create a custom repository to your project, you could do like this:

```
git remote add origin https://your-repo.git
```

# License

MIT
